function defineEnding(number) {
  if (number % 10 === 1) {
    ending = '';
  }
  if (2 <= number % 10 && number % 10 <= 4) {
    ending = 'а';
  }
  if ((5 <= number % 10 && number % 10 <= 9) || number % 10 === 0) {
    ending = 'ов';
  }
  console.log(ending);
  return ending;
}
