export default [
  {
    id: 1,
    categoryId: 1,
    title: 'Двигатель MAN V12-2000',
    price: 3690000,
    image: 'myIMG/man.jpg',
  },
  {
    id: 2,
    categoryId: 2,
    title: 'Робот KUKA 327',
    price: 1690000,
    image: 'myIMG/kuka.avif',
  },
  {
    id: 3,
    categoryId: 3,
    title: 'Дробилка вилковая',
    price: 2500000,
    image: 'myIMG/drob.jpg',
  },
  {
    id: 4,
    categoryId: 3,
    title: 'Дробилка вилковая',
    price: 1500000,
    image: 'myIMG/drob.jpg',
  },
  {
    id: 5,
    categoryId: 1,
    title: 'Двигатель MAN V12-2000',
    price: 2690000,
    image: 'myIMG/man.jpg',
  },
  {
    id: 6,
    categoryId: 2,
    title: 'Робот KUKA 2',
    price: 690000,
    image: 'myIMG/kuka.avif',
  },
];
