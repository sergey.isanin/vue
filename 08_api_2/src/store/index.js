import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { API_BASE_URL } from '@/config'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cartProducts: [],
    userAccessKey: null,
    orderInfo: null,
  },
  mutations: {
    updateOrderInfo(state, orderInfo) {
      state.orderInfo = orderInfo
    },
    updateCartProducts(state, items) {
      state.cartProducts = items
    },
    updateUserAccessKey(state, accessKey) {
      state.userAccessKey = accessKey
    },
    resetCart(state) {
      state.cartProducts = []
      state.cartProductsData = []
    },
  },
  getters: {
    cartProducts(state) {
      return state.cartProducts.map((cartItem) => ({
        ...cartItem,
        product: { ...cartItem.product, image: cartItem.product.image.file.url },
      }))
    },
    cartTotalPrice(state, getters) {
      return getters.cartProducts.reduce((acc, item) => item.product.price * item.quantity + acc, 0)
    },
  },
  actions: {
    loadOrderInfo(context, orderId) {
      return axios
        .get(API_BASE_URL + '/api/orders/' + orderId, {
          params: {
            userAccessKey: context.state.userAccessKey,
          },
        })
        .then((response) => {
          context.commit('updateOrderInfo', response.data)
        })
    },
    loadCart(context) {
      return axios
        .get(API_BASE_URL + '/api/baskets', {
          params: {
            userAccessKey: context.state.userAccessKey,
          },
        })
        .then((response) => {
          if (!context.state.userAccessKey) {
            localStorage.setItem('userAccessKey', response.data.user.accessKey)
            context.commit('updateUserAccessKey', response.data.user.accessKey)
          }
          context.commit('updateCartProducts', response.data.items)
        })
    },
    addProductToCart(context, { productId, amount }) {
      return axios
        .post(
          API_BASE_URL + '/api/baskets/products',
          {
            productId: productId,
            quantity: amount,
          },
          { params: { userAccessKey: context.state.userAccessKey } }
        )
        .then((response) => {
          context.commit('updateCartProducts', response.data.items)
        })
    },
    updateCartProductAmount(context, { productId, quantity }) {
      if (quantity < 1) {
        return
      }

      return axios
        .put(
          API_BASE_URL + '/api/baskets/products',
          {
            productId,
            quantity,
          },
          {
            params: {
              userAccessKey: context.state.userAccessKey,
            },
          }
        )
        .then((response) => {
          context.commit('updateCartProducts', response.data.items)
        })
    },
    deleteCartProduct(context, productId) {
      return axios
        .delete(API_BASE_URL + '/api/baskets/products', {
          params: { userAccessKey: context.state.userAccessKey },
          data: { productId },
        })
        .then((response) => {
          context.commit('updateCartProducts', response.data.items)
        })
    },
  },
})
