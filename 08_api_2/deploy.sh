#!/usr/bin/env sh
# остановить публикацию при ошибках 
set -е

# сборка приложения 
npm run build

# переход в каталог сборки 
cd dist


# инициализация репозитория и загрузка кода в GitHub
git init
git add -A
git commit -m 'deploy'

# git push -f git@github.com:creonit-dev/vue-study.git master:gh-pages
git push -f git@github.com:i5anin/skillbox_vue-app.git master:gh-pages

cd -